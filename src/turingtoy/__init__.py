from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine( machine: Dict, input_: str, steps: Optional[int] = None,) -> Tuple[str, List, bool]:
	currentstate = machine["start state"]
	position = 0
	memory = input_
	blanck = machine["blank"]
	history = []	
	while True:
		if position >= len(memory):
			memory += blanck
		if position < 0:
			memory = blanck + memory
			position = 0
		reading = memory[position]
		transition = machine["table"][currentstate][reading]
		new_history = {"state":currentstate, "reading":reading, "position":position, "memory":memory, "transition":transition}
		history.append(new_history)
		if type(transition) == str:
			if transition == "L":
				position = position -1
			else:
				position += 1
		else:
			for element in transition:
				val = transition[element]
				if element == "L":
					currentstate = val
					position = position -1
				elif element == "R":
					currentstate = val
					position +=1
				else:
					conver = list(memory)
					conver[position] = val
					memory = "".join(conver)

				
		if currentstate == "".join(machine["final states"]):
			break
	memory = memory.strip(blanck)
	return (memory.strip("+"), history, True)
		

